# Project setup

## Install dependencies

```bash
nvm use
pnpm install
```

## Run the project

```bash
pnpm run dev
```

or

```bash
pnpm run build
pnpm run start
```
