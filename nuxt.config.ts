// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },
    modules: ["nuxt-headlessui", "@unocss/nuxt", "@nuxt/image"],
    css: ["~/assets/scss/theme.scss"],
    app: {
        head: {
            title: "Maison Liva",
        },
    },
    image: {
        provider: 'netlify',
        netlify: {
          baseURl: "public"
        }
      }
});
