import {
    defineConfig,
    presetAttributify,
    presetIcons,
    presetTypography,
    presetUno,
    presetWebFonts,
    transformerDirectives,
} from "unocss";

export default defineConfig({
    transformers: [transformerDirectives()],
    presets: [
        presetAttributify(),
        presetUno(),
        presetIcons(),
        presetTypography(),
        presetWebFonts({
            provider: "google",
            fonts: {
                sans: "Nunito:300,400",
            },
        }),
    ],
    theme: {
        colors: {
            blue: "#2e3192",
            red: "#ed1c24",
            green: "#095234",
            beige: "#fef2e3",
            yellow: "#FFCC00",
        },
        fontFamily: { tan: "TAN-NIMBUS" },
    },
});
